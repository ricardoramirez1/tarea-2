# Tarea 2

En esta tarea debemos vincular **BitBucket** como repositorio remoto y modificarlo.

Tenemos vinculado un repositorio de GitLab como repositorio remoto debido a la Tarea 1, por lo que debemos modificar la URL de la siguiente manera:

> git remote set-url origin https://ricardoramirez1-admin@bitbucket.org/ricardoramirez1/tarea-2.git

Una vez hecho, clonamos el repositorio de BitBucket:

> git clone https://ricardoramirez1-admin@bitbucket.com/ricardoramirez1/tarea-2.git

Ya tenemos acceso al repositorio. Ahora solo hace falta añadir el archivo **index.html** y modificar el archivo **README.md** tal y como se hizo en la Tarea 1.
Los datos se guardan de la siguiente manera:

1. Añado **index.html** usando *git add index.html*

2. Añado los cambios usando *git commit -m "COMENTARIO"*.

3. Modifico **README.md** con la información necesaria.

4. Añado los cambios igual que en el paso 2.

5. Confirmo los cambios usando *git push origin main*.

Con todo esto, hemos subido el archivo y realizado los cambios satisfactoriamente.
